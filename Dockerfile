FROM ubuntu:xenial

RUN apt-get update && apt-get install -y texlive texlive-latex-extra texlive-lang-german latexmk
